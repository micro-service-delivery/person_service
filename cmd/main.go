package main

import (
	"context"
	"fmt"
	"log"
	"net"
	"person/config"
	"person/grpc"
	grpc_client "person/grpc/client"

	"person/packages/logger"
	"person/storage/memory"
)

func main() {
	cfg := config.Load()
	lg := logger.NewLogger(cfg.Environment, "debug")
	strg, err := memory.NewStorage(context.Background(), *cfg)
	if err != nil {
		log.Fatalf("failed to connect to database: %v", err)
	}
	clients, err := grpc_client.New(*cfg)
	if err != nil {
		log.Fatalf("failed to connect to services: %v", err)
	}
	s := grpc.SetUpServer(lg, strg, clients)
	lis, err := net.Listen("tcp", fmt.Sprintf(":%d", 50052))
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	log.Printf("server listening at %v", lis.Addr())

	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}
