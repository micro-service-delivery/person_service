// Code generated by protoc-gen-go-grpc. DO NOT EDIT.
// versions:
// - protoc-gen-go-grpc v1.2.0
// - protoc             v3.12.4
// source: client.proto

package person_service

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.32.0 or later.
const _ = grpc.SupportPackageIsVersion7

// ClientServiceClient is the client API for ClientService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type ClientServiceClient interface {
	// Sends a greeting
	Create(ctx context.Context, in *CreateClient, opts ...grpc.CallOption) (*IdReqRes, error)
	Update(ctx context.Context, in *Client, opts ...grpc.CallOption) (*ResponseString, error)
	Get(ctx context.Context, in *IdReqRes, opts ...grpc.CallOption) (*Client, error)
	Delete(ctx context.Context, in *IdReqRes, opts ...grpc.CallOption) (*ResponseString, error)
	GetAll(ctx context.Context, in *GetAllClientRequest, opts ...grpc.CallOption) (*GetAllClientResponse, error)
}

type clientServiceClient struct {
	cc grpc.ClientConnInterface
}

func NewClientServiceClient(cc grpc.ClientConnInterface) ClientServiceClient {
	return &clientServiceClient{cc}
}

func (c *clientServiceClient) Create(ctx context.Context, in *CreateClient, opts ...grpc.CallOption) (*IdReqRes, error) {
	out := new(IdReqRes)
	err := c.cc.Invoke(ctx, "/person_service.ClientService/Create", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *clientServiceClient) Update(ctx context.Context, in *Client, opts ...grpc.CallOption) (*ResponseString, error) {
	out := new(ResponseString)
	err := c.cc.Invoke(ctx, "/person_service.ClientService/Update", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *clientServiceClient) Get(ctx context.Context, in *IdReqRes, opts ...grpc.CallOption) (*Client, error) {
	out := new(Client)
	err := c.cc.Invoke(ctx, "/person_service.ClientService/Get", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *clientServiceClient) Delete(ctx context.Context, in *IdReqRes, opts ...grpc.CallOption) (*ResponseString, error) {
	out := new(ResponseString)
	err := c.cc.Invoke(ctx, "/person_service.ClientService/Delete", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *clientServiceClient) GetAll(ctx context.Context, in *GetAllClientRequest, opts ...grpc.CallOption) (*GetAllClientResponse, error) {
	out := new(GetAllClientResponse)
	err := c.cc.Invoke(ctx, "/person_service.ClientService/GetAll", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// ClientServiceServer is the server API for ClientService service.
// All implementations should embed UnimplementedClientServiceServer
// for forward compatibility
type ClientServiceServer interface {
	// Sends a greeting
	Create(context.Context, *CreateClient) (*IdReqRes, error)
	Update(context.Context, *Client) (*ResponseString, error)
	Get(context.Context, *IdReqRes) (*Client, error)
	Delete(context.Context, *IdReqRes) (*ResponseString, error)
	GetAll(context.Context, *GetAllClientRequest) (*GetAllClientResponse, error)
}

// UnimplementedClientServiceServer should be embedded to have forward compatible implementations.
type UnimplementedClientServiceServer struct {
}

func (UnimplementedClientServiceServer) Create(context.Context, *CreateClient) (*IdReqRes, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Create not implemented")
}
func (UnimplementedClientServiceServer) Update(context.Context, *Client) (*ResponseString, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Update not implemented")
}
func (UnimplementedClientServiceServer) Get(context.Context, *IdReqRes) (*Client, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Get not implemented")
}
func (UnimplementedClientServiceServer) Delete(context.Context, *IdReqRes) (*ResponseString, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Delete not implemented")
}
func (UnimplementedClientServiceServer) GetAll(context.Context, *GetAllClientRequest) (*GetAllClientResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetAll not implemented")
}

// UnsafeClientServiceServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to ClientServiceServer will
// result in compilation errors.
type UnsafeClientServiceServer interface {
	mustEmbedUnimplementedClientServiceServer()
}

func RegisterClientServiceServer(s grpc.ServiceRegistrar, srv ClientServiceServer) {
	s.RegisterService(&ClientService_ServiceDesc, srv)
}

func _ClientService_Create_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(CreateClient)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ClientServiceServer).Create(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/person_service.ClientService/Create",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ClientServiceServer).Create(ctx, req.(*CreateClient))
	}
	return interceptor(ctx, in, info, handler)
}

func _ClientService_Update_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(Client)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ClientServiceServer).Update(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/person_service.ClientService/Update",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ClientServiceServer).Update(ctx, req.(*Client))
	}
	return interceptor(ctx, in, info, handler)
}

func _ClientService_Get_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(IdReqRes)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ClientServiceServer).Get(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/person_service.ClientService/Get",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ClientServiceServer).Get(ctx, req.(*IdReqRes))
	}
	return interceptor(ctx, in, info, handler)
}

func _ClientService_Delete_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(IdReqRes)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ClientServiceServer).Delete(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/person_service.ClientService/Delete",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ClientServiceServer).Delete(ctx, req.(*IdReqRes))
	}
	return interceptor(ctx, in, info, handler)
}

func _ClientService_GetAll_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetAllClientRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ClientServiceServer).GetAll(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/person_service.ClientService/GetAll",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ClientServiceServer).GetAll(ctx, req.(*GetAllClientRequest))
	}
	return interceptor(ctx, in, info, handler)
}

// ClientService_ServiceDesc is the grpc.ServiceDesc for ClientService service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var ClientService_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "person_service.ClientService",
	HandlerType: (*ClientServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "Create",
			Handler:    _ClientService_Create_Handler,
		},
		{
			MethodName: "Update",
			Handler:    _ClientService_Update_Handler,
		},
		{
			MethodName: "Get",
			Handler:    _ClientService_Get_Handler,
		},
		{
			MethodName: "Delete",
			Handler:    _ClientService_Delete_Handler,
		},
		{
			MethodName: "GetAll",
			Handler:    _ClientService_GetAll_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "client.proto",
}
