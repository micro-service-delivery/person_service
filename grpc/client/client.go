package grpc_client

import (
	"fmt"
	"person/config"
	"person/genproto/catalog_service"
	"person/genproto/order_service"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

// GrpcClientI ...
type GrpcClientI interface {
	OrderService() order_service.OrderServiceClient
	CategoryService() catalog_service.CategoryServiceClient
}

// GrpcClient ...
type GrpcClient struct {
	cfg         config.Config
	connections map[string]interface{}
}

// New ...
func New(cfg config.Config) (*GrpcClient, error) {

	connCatalog, err := grpc.Dial(fmt.Sprintf("%s:%d", cfg.CatalogServiceHost, cfg.CatalogServisePort), grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		return nil, fmt.Errorf("catalog service dial host: %s port:%d err: %s",
			cfg.CatalogServiceHost, cfg.CatalogServisePort, err)
	}

	connOrder, err := grpc.Dial(fmt.Sprintf("%s:%d", cfg.OrderServiceHost, cfg.OrderServisePort), grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		return nil, fmt.Errorf("order service dial host: %s port:%d err: %s",
			cfg.OrderServiceHost, cfg.OrderServisePort, err)
	}

	return &GrpcClient{
		cfg: cfg,
		connections: map[string]interface{}{
			"order_service":   order_service.NewOrderServiceClient(connOrder),
			"catalog_service": catalog_service.NewCategoryServiceClient(connCatalog),
			// "tarif_service":              staff_service.NewTarifServerClient(connStaff),
			// "sale_service":               sale_service.NewSaleServerClient(connSale),
			// "sale_product_service":       sale_service.NewSaleProductServerClient(connSale),
			// "branch_transaction_service": sale_service.NewBranchTransactionServerClient(connSale),
			// "transaction_service":        sale_service.NewTransactionServerClient(connSale),
		},
	}, nil
}

func (g *GrpcClient) OrderService() order_service.OrderServiceClient {
	return g.connections["drder_service"].(order_service.OrderServiceClient)
}

func (g *GrpcClient) CategoryService() catalog_service.CategoryServiceClient {
	return g.connections["catalog_service"].(catalog_service.CategoryServiceClient)
}

// func (g *GrpcClient) StaffService() staff_service.StaffServerClient {
// 	return g.connections["staff_service"].(staff_service.StaffServerClient)
// }

// func (g *GrpcClient) SaleService() sale_service.SaleServerClient {
// 	return g.connections["sale_service"].(sale_service.SaleServerClient)
// }

// func (g *GrpcClient) SaleProductService() sale_service.SaleProductServerClient {
// 	return g.connections["sale_product_service"].(sale_service.SaleProductServerClient)
// }

// func (g *GrpcClient) BranchTransactionService() sale_service.BranchTransactionServerClient {
// 	return g.connections["branch_transaction_service"].(sale_service.BranchTransactionServerClient)
// }

// func (g *GrpcClient) TransactionService() sale_service.TransactionServerClient {
// 	return g.connections["transaction_service"].(sale_service.TransactionServerClient)
// }
