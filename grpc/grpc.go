package grpc

import (
	"person/genproto/person_service"
	grpc_client "person/grpc/client"
	"person/grpc/service"
	"person/packages/logger"
	"person/storage"

	"google.golang.org/grpc"
)

func SetUpServer(log logger.LoggerI, strg storage.StoregeI, grpcClient grpc_client.GrpcClientI) *grpc.Server {
	s := grpc.NewServer()
	person_service.RegisterBranchServiceServer(s, service.NewBranchService(log, strg, grpcClient))
	person_service.RegisterClientServiceServer(s, service.NewClientService(log, strg, grpcClient))
	person_service.RegisterCourierServiceServer(s, service.NewCourierService(log, strg, grpcClient))
	person_service.RegisterUserServiceServer(s, service.NewUserService(log, strg, grpcClient))
	return s
}
