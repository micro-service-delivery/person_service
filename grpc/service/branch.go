package service

import (
	"context"

	"person/genproto/person_service"
	grpc_client "person/grpc/client"
	"person/packages/logger"
	"person/storage"
)

type BranchService struct {
	logger  logger.LoggerI
	storage storage.StoregeI
	clients grpc_client.GrpcClientI
	person_service.UnimplementedBranchServiceServer
}

func NewBranchService(log logger.LoggerI, strg storage.StoregeI, grpcClients grpc_client.GrpcClientI) *BranchService {
	return &BranchService{
		logger:  log,
		storage: strg,
		clients: grpcClients,
	}
}

func (t *BranchService) Create(ctx context.Context, req *person_service.CreateBranch) (*person_service.IdReqRes, error) {
	id, err := t.storage.Branch().Create(ctx, req)
	if err != nil {
		return nil, err
	}

	return &person_service.IdReqRes{Id: id}, nil
}

func (t *BranchService) Update(ctx context.Context, req *person_service.Branch) (*person_service.ResponseString, error) {
	str, err := t.storage.Branch().Update(ctx, req)
	if err != nil {
		return nil, err
	}

	return &person_service.ResponseString{Text: str}, nil
}

func (t *BranchService) Get(ctx context.Context, req *person_service.IdReqRes) (*person_service.Branch, error) {
	branch, err := t.storage.Branch().Get(ctx, req)
	if err != nil {
		return nil, err
	}

	return branch, nil
}

func (t *BranchService) GetAll(ctx context.Context, req *person_service.GetAllBranchRequest) (*person_service.GetAllBranchResponse, error) {
	branches, err := t.storage.Branch().GetAll(ctx, req)
	if err != nil {
		return nil, err
	}

	return branches, nil
}

func (t *BranchService) Delete(ctx context.Context, req *person_service.IdReqRes) (*person_service.ResponseString, error) {
	text, err := t.storage.Branch().Delete(ctx, req)
	if err != nil {
		return nil, err
	}

	return &person_service.ResponseString{Text: text}, nil
}

func (t *BranchService) GetAllActive(ctx context.Context, req *person_service.GetAllActiveBranchRequest) (*person_service.GetAllBranchResponse, error) {
	branches, err := t.storage.Branch().GetAllActive(ctx, req)
	if err != nil {
		return nil, err
	}

	return branches, nil
}
