package service

import (
	"person/genproto/person_service"
	grpc_client "person/grpc/client"

	"context"
	"person/packages/logger"
	"person/storage"
)

type ClientService struct {
	logger  logger.LoggerI
	storage storage.StoregeI
	clients grpc_client.GrpcClientI
	person_service.UnimplementedClientServiceServer
}

func NewClientService(log logger.LoggerI, strg storage.StoregeI, grpcClients grpc_client.GrpcClientI) *ClientService {
	return &ClientService{
		logger:  log,
		storage: strg,
		clients: grpcClients,
	}
}

func (t *ClientService) Create(ctx context.Context, req *person_service.CreateClient) (*person_service.IdReqRes, error) {
	id, err := t.storage.Client().Create(ctx, req)
	if err != nil {
		return nil, err
	}

	return &person_service.IdReqRes{Id: id}, nil
}

func (t *ClientService) Update(ctx context.Context, req *person_service.Client) (*person_service.ResponseString, error) {
	str, err := t.storage.Client().Update(ctx, req)
	if err != nil {
		return nil, err
	}

	return &person_service.ResponseString{Text: str}, nil
}

func (t *ClientService) Get(ctx context.Context, req *person_service.IdReqRes) (*person_service.Client, error) {
	client, err := t.storage.Client().Get(ctx, req)
	if err != nil {
		return nil, err
	}

	return client, nil
}

func (t *ClientService) GetAll(ctx context.Context, req *person_service.GetAllClientRequest) (*person_service.GetAllClientResponse, error) {
	clients, err := t.storage.Client().GetAll(ctx, req)
	if err != nil {
		return nil, err
	}

	return clients, nil
}

func (t *ClientService) Delete(ctx context.Context, req *person_service.IdReqRes) (*person_service.ResponseString, error) {
	text, err := t.storage.Client().Delete(ctx, req)
	if err != nil {
		return nil, err
	}

	return &person_service.ResponseString{Text: text}, nil
}
