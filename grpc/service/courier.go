package service

import (
	"context"

	"person/genproto/person_service"
	grpc_client "person/grpc/client"
	"person/packages/logger"
	"person/storage"
)

type CourierService struct {
	logger  logger.LoggerI
	storage storage.StoregeI
	clients grpc_client.GrpcClientI
	person_service.UnimplementedCourierServiceServer
}

func NewCourierService(log logger.LoggerI, strg storage.StoregeI, grpcClients grpc_client.GrpcClientI) *CourierService {
	return &CourierService{
		logger:  log,
		storage: strg,
		clients: grpcClients,
	}
}

func (t *CourierService) Create(ctx context.Context, req *person_service.CreateCourier) (*person_service.IdReqRes, error) {
	id, err := t.storage.Courier().Create(ctx, req)
	if err != nil {
		return nil, err
	}

	return &person_service.IdReqRes{Id: id}, nil
}

func (t *CourierService) Update(ctx context.Context, req *person_service.Courier) (*person_service.ResponseString, error) {
	str, err := t.storage.Courier().Update(ctx, req)
	if err != nil {
		return nil, err
	}

	return &person_service.ResponseString{Text: str}, nil
}

func (t *CourierService) Get(ctx context.Context, req *person_service.IdReqRes) (*person_service.Courier, error) {
	courier, err := t.storage.Courier().Get(ctx, req)
	if err != nil {
		return nil, err
	}

	return courier, nil
}

func (t *CourierService) GetAll(ctx context.Context, req *person_service.GetAllCourierRequest) (*person_service.GetAllCourierResponse, error) {
	couriers, err := t.storage.Courier().GetAll(ctx, req)
	if err != nil {
		return nil, err
	}

	return couriers, nil
}

func (t *CourierService) Delete(ctx context.Context, req *person_service.IdReqRes) (*person_service.ResponseString, error) {
	text, err := t.storage.Courier().Delete(ctx, req)
	if err != nil {
		return nil, err
	}

	return &person_service.ResponseString{Text: text}, nil
}

func (t *CourierService) GetCourierByUserName(ctx context.Context, req *person_service.GetByUserName) (*person_service.Courier, error) {
	courier, err := t.storage.Courier().GetCourierByUserName(ctx, req)
	if err != nil {
		return nil, err
	}

	return courier, nil
}
