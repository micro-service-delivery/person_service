package service

import (
	"context"
	"person/genproto/person_service"
	grpc_client "person/grpc/client"
	"person/packages/logger"
	"person/storage"
)

type UserService struct {
	logger  logger.LoggerI
	storage storage.StoregeI
	clients grpc_client.GrpcClientI
	person_service.UnimplementedUserServiceServer
}

func NewUserService(log logger.LoggerI, strg storage.StoregeI, grpcClients grpc_client.GrpcClientI) *UserService {
	return &UserService{
		logger:  log,
		storage: strg,
		clients: grpcClients,
	}
}

func (t *UserService) Create(ctx context.Context, req *person_service.CreateUser) (*person_service.IdReqRes, error) {
	id, err := t.storage.User().Create(ctx, req)
	if err != nil {
		return nil, err
	}

	return &person_service.IdReqRes{Id: id}, nil
}

func (t *UserService) Update(ctx context.Context, req *person_service.User) (*person_service.ResponseString, error) {
	str, err := t.storage.User().Update(ctx, req)
	if err != nil {
		return nil, err
	}

	return &person_service.ResponseString{Text: str}, nil
}

func (t *UserService) Get(ctx context.Context, req *person_service.IdReqRes) (*person_service.User, error) {
	user, err := t.storage.User().Get(ctx, req)
	if err != nil {
		return nil, err
	}

	return user, nil
}

func (t *UserService) GetAll(ctx context.Context, req *person_service.GetAllUserRequest) (*person_service.GetAllUserResponse, error) {
	users, err := t.storage.User().GetAll(ctx, req)
	if err != nil {
		return nil, err
	}

	return users, nil
}

func (t *UserService) Delete(ctx context.Context, req *person_service.IdReqRes) (*person_service.ResponseString, error) {
	text, err := t.storage.User().Delete(ctx, req)
	if err != nil {
		return nil, err
	}

	return &person_service.ResponseString{Text: text}, nil
}

func (t *UserService) GetUserByUserName(ctx context.Context, req *person_service.GetByUserName) (*person_service.User, error) {
	user, err := t.storage.User().GetUserByUserName(ctx, req)
	if err != nil {
		return nil, err
	}

	return user, nil
}
