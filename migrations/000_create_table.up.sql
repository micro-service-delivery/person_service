CREATE TYPE "type_discount" AS ENUM (
  'fixed',
  'percent'
);

CREATE TABLE "users" (
  "id" uuid PRIMARY KEY,
  "first_name" varchar,
  "last_name" varchar,
  "phone" varchar,
  "active" bool DEFAULT true,
  "login" varchar,
  "password" varchar,
  "created_at" timestamp DEFAULT (now()),
  "updated_at" timestamp DEFAULT (now()),
  "deleted_at" timestamp
);

CREATE TABLE "branches" (
  "id" uuid PRIMARY KEY,
  "user_id" uuid,
  "name" varchar NOT NULL,
  "phone" varchar NOT NULL,
  "photo" varchar,
  "delivery_tarif_id" uuid,
  "work_start_hour" time,
  "work_end_hour" time,
  "destination" varchar,
  "address" varchar,
  "active" bool DEFAULT true,
  "created_at" timestamp DEFAULT (now()),
  "updated_at" timestamp DEFAULT (now()),
  "deleted_at" timestamp
);

CREATE TABLE "clients" (
  "id" uuid PRIMARY KEY,
  "first_name" varchar NOT NULL,
  "last_name" varchar,
  "phone" varchar NOT NULL,
  "photo" varchar,
  "birth_date" date,
  "last_order_date" timestamp,
  "total_orders_sum" numeric,
  "total_orders_count" integer,
  "discount_type" type_discount,
  "discount_amount" numeric,
  "created_at" timestamp DEFAULT (now()),
  "updated_at" timestamp DEFAULT (now()),
  "deleted_at" timestamp
);

CREATE TABLE "couriers" (
  "id" uuid PRIMARY KEY,
  "branch_id" uuid,
  "first_name" varchar NOT NULL,
  "last_name" varchar,
  "phone" varchar NOT NULL,
  "active" bool DEFAULT true,
  "login" varchar,
  "password" varchar,
  "max_order_count" integer,
  "created_at" timestamp DEFAULT (now()),
  "updated_at" timestamp DEFAULT (now()),
  "deleted_at" timestamp
);

ALTER TABLE "branches" ADD FOREIGN KEY ("user_id") REFERENCES "users" ("id");

ALTER TABLE "couriers" ADD FOREIGN KEY ("branch_id") REFERENCES "branches" ("id");

