package memory

import (
	"context"
	"fmt"

	"person/genproto/person_service"

	"github.com/google/uuid"
	"github.com/jackc/pgx"
	"github.com/jackc/pgx/v4/pgxpool"
)

type branchRepo struct {
	db *pgxpool.Pool
}

func NewBranchRepo(db *pgxpool.Pool) *branchRepo {

	return &branchRepo{
		db: db,
	}

}

func (c *branchRepo) Create(ctx context.Context, req *person_service.CreateBranch) (string, error) {

	var id = uuid.NewString()

	query := `
	INSERT INTO
		branches (id,name,user_id,phone,photo,delivery_tarif_id,work_start_hour,work_end_hour,destination,address)
	VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10)`

	_, err := c.db.Exec(ctx, query,
		id,
		req.Name,
		req.UserId,
		req.Phone,
		req.Photo,
		req.DeliveryTarifId,
		req.WorkStartHour,
		req.WorkEndHour,
		req.Destination,
		req.Address,
	)

	if err != nil {
		fmt.Println("error:", err.Error())
		return "", err
	}

	return id, nil
}

func (c *branchRepo) Update(ctx context.Context, req *person_service.Branch) (string, error) {

	query := `
	UPDATE
		branches
	SET
		name=$2,user_id=$3,phone=$4,photo=$5,delivery_tarif_id=$6,work_start_hour=$7,work_end_hour=$8,destination=$9,address=$10,updated_at=NOW()
	WHERE
		id=$1`

	resp, err := c.db.Exec(ctx, query,
		req.Id,
		req.Name,
		req.UserId,
		req.Phone,
		req.Photo,
		req.DeliveryTarifId,
		req.WorkStartHour,
		req.WorkEndHour,
		req.Destination,
		req.Address,
	)

	if err != nil {
		return "", err
	}

	if resp.RowsAffected() == 0 {
		return "", pgx.ErrNoRows
	}

	return "OK", nil
}

func (c *branchRepo) Get(ctx context.Context, req *person_service.IdReqRes) (*person_service.Branch, error) {

	query := `
	SELECT
	id,
	name,
	user_id,
	phone,
	photo,
	delivery_tarif_id,
	work_start_hour::text,
	work_end_hour::text,
	destination,
	address,
	active,
	created_at::text,
	updated_at::text
	FROM branches
	WHERE id=$1`

	resp := c.db.QueryRow(ctx, query, req.Id)

	var branch person_service.Branch

	err := resp.Scan(
		&branch.Id,
		&branch.Name,
		&branch.UserId,
		&branch.Phone,
		&branch.Photo,
		&branch.DeliveryTarifId,
		&branch.WorkStartHour,
		&branch.WorkEndHour,
		&branch.Destination,
		&branch.Address,
		&branch.Active,
		&branch.CreatedAt,
		&branch.UpdatedAt,
	)

	if err != nil {
		return &person_service.Branch{}, err
	}

	return &branch, nil
}

func (c *branchRepo) Delete(ctx context.Context, req *person_service.IdReqRes) (string, error) {

	query := `UPDATE branches SET deleted_at=NOW() WHERE id = $1`

	resp, err := c.db.Exec(ctx, query,
		req.Id,
	)

	if err != nil {
		return "", err
	}
	if resp.RowsAffected() == 0 {
		return "", pgx.ErrNoRows
	}

	return "Deleted suc", nil
}

func (c *branchRepo) GetAll(ctx context.Context, req *person_service.GetAllBranchRequest) (resp *person_service.GetAllBranchResponse, err error) {

	var (
		filter  = " WHERE deleted_at IS NULL AND true "
		offsetQ = " OFFSET 0 "
		limit   = " LIMIT 10 "
		offset  = (req.GetPage() - 1) * req.GetLimit()
	)

	info := `
	SELECT
	id,
	name,
	user_id,
	phone,
	photo,
	delivery_tarif_id,
	work_start_hour::text,
	work_end_hour::text,
	destination,
	address,
	active,
	created_at::text,
	updated_at::text
	FROM branches
	`

	var count int

	cQ := `
	SELECT
		COUNT(*)
	FROM 
		branches
	`
	if req.GetName() != "" {
		filter += ` AND first_name ILIKE ` + "'%" + req.GetName() + "%'"
	}

	filter += ` AND created_at BETWEEN '` + req.FromDate + `' AND '` + req.ToDate + `' `

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}
	if offset > 0 {
		offsetQ = fmt.Sprintf(" OFFSET %d", offset)
	}

	query := info + filter + limit + offsetQ
	countQuery := cQ + filter

	rows, err := c.db.Query(ctx, query)
	if err != nil {
		return &person_service.GetAllBranchResponse{}, err
	}

	err = c.db.QueryRow(context.Background(), countQuery).Scan(&count)

	if err != nil {
		return &person_service.GetAllBranchResponse{}, err
	}

	defer rows.Close()

	result := []*person_service.Branch{}

	for rows.Next() {

		var branch person_service.Branch

		err := rows.Scan(
			&branch.Id,
			&branch.Name,
			&branch.UserId,
			&branch.Phone,
			&branch.Photo,
			&branch.DeliveryTarifId,
			&branch.WorkStartHour,
			&branch.WorkEndHour,
			&branch.Destination,
			&branch.Address,
			&branch.Active,
			&branch.CreatedAt,
			&branch.UpdatedAt,
		)
		if err != nil {
			return &person_service.GetAllBranchResponse{}, err
		}

		result = append(result, &branch)

	}

	return &person_service.GetAllBranchResponse{Branches: result, Count: int64(count)}, nil

}

func (c *branchRepo) GetAllActive(ctx context.Context, req *person_service.GetAllActiveBranchRequest) (resp *person_service.GetAllBranchResponse, err error) {

	var (
		filter  = " WHERE deleted_at IS NULL AND active=true "
		offsetQ = " OFFSET 0 "
		limit   = " LIMIT 10 "
		offset  = (req.GetPage() - 1) * req.GetLimit()
	)

	info := `
	SELECT
	id,
	name,
	user_id,
	phone,
	photo,
	delivery_tarif_id,
	work_start_hour::text,
	work_end_hour::text,
	destination,
	address,
	active,
	created_at::text,
	updated_at::text
	FROM branches
	`

	var count int

	cQ := `
	SELECT
		COUNT(*)
	FROM 
		branches
	`

	fmt.Println(req.Date)

	filter += ` AND '` + req.Date + `'::time BETWEEN work_start_hour AND work_end_hour `

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}
	if offset > 0 {
		offsetQ = fmt.Sprintf(" OFFSET %d", offset)
	}

	query := info + filter + limit + offsetQ
	countQuery := cQ + filter

	rows, err := c.db.Query(ctx, query)
	if err != nil {
		return &person_service.GetAllBranchResponse{}, err
	}

	err = c.db.QueryRow(context.Background(), countQuery).Scan(&count)

	if err != nil {
		return &person_service.GetAllBranchResponse{}, err
	}

	defer rows.Close()

	result := []*person_service.Branch{}

	for rows.Next() {

		var branch person_service.Branch

		err := rows.Scan(
			&branch.Id,
			&branch.Name,
			&branch.UserId,
			&branch.Phone,
			&branch.Photo,
			&branch.DeliveryTarifId,
			&branch.WorkStartHour,
			&branch.WorkEndHour,
			&branch.Destination,
			&branch.Address,
			&branch.Active,
			&branch.CreatedAt,
			&branch.UpdatedAt,
		)
		if err != nil {
			return &person_service.GetAllBranchResponse{}, err
		}

		result = append(result, &branch)

	}

	return &person_service.GetAllBranchResponse{Branches: result, Count: int64(count)}, nil

}
