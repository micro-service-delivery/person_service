package memory

import (
	"context"
	"fmt"
	"person/genproto/person_service"

	"github.com/google/uuid"
	"github.com/jackc/pgx"
	"github.com/jackc/pgx/v4/pgxpool"
)

type clientRepo struct {
	db *pgxpool.Pool
}

func NewClientRepo(db *pgxpool.Pool) *clientRepo {

	return &clientRepo{
		db: db,
	}

}

func (c *clientRepo) Create(ctx context.Context, req *person_service.CreateClient) (string, error) {

	var id = uuid.NewString()

	query := `
	INSERT INTO
		clients (id,first_name,last_name,phone,photo,birth_date,last_order_date,discount_type,total_orders_sum,total_orders_count,discount_amount)
	VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11)`

	_, err := c.db.Exec(ctx, query,
		id,
		req.FirstName,
		req.LastName,
		req.Phone,
		req.Photo,
		req.BirthDate,
		req.LastOrderDate,
		req.DiscountType,
		req.TotalOrderSum,
		req.TotalOrderCount,
		req.DiscountAmount,
	)

	if err != nil {
		fmt.Println("error:", err.Error())
		return "", err
	}

	return id, nil
}

func (c *clientRepo) Update(ctx context.Context, req *person_service.Client) (string, error) {

	query := `
	UPDATE
		clients
	SET
		first_name=$2,last_name=$3,phone=$4,photo=$5,birth_date=$6,last_order_date=$7,discount_type=$8,total_orders_sum=$9,total_orders_count=$10,discount_amount=$11,updated_at=NOW()
	WHERE
		id=$1`

	resp, err := c.db.Exec(ctx, query,
		req.Id,
		req.FirstName,
		req.LastName,
		req.Phone,
		req.Photo,
		req.BirthDate,
		req.LastOrderDate,
		req.DiscountType,
		req.TotalOrderSum,
		req.TotalOrderCount,
		req.DiscountAmount,
	)

	if err != nil {
		return "", err
	}

	if resp.RowsAffected() == 0 {
		return "", pgx.ErrNoRows
	}

	return "OK", nil
}

func (c *clientRepo) Get(ctx context.Context, req *person_service.IdReqRes) (*person_service.Client, error) {

	query := `
	SELECT
	id,
	first_name,
	last_name,
	phone,
	photo,
	birth_date::text,
	last_order_date::text,
	discount_type,
	total_orders_sum,
	total_orders_count,
	discount_amount,
	created_at::text,
	updated_at::text
	FROM clients
	WHERE id=$1`

	resp := c.db.QueryRow(ctx, query, req.Id)

	var client person_service.Client

	err := resp.Scan(
		&client.Id,
		&client.FirstName,
		&client.LastName,
		&client.Phone,
		&client.Photo,
		&client.BirthDate,
		&client.LastOrderDate,
		&client.DiscountType,
		&client.TotalOrderSum,
		&client.TotalOrderCount,
		&client.DiscountAmount,
		&client.CreatedAt,
		&client.UpdatedAt,
	)

	if err != nil {
		return &person_service.Client{}, err
	}

	return &client, nil
}

func (c *clientRepo) Delete(ctx context.Context, req *person_service.IdReqRes) (string, error) {

	query := `UPDATE clients SET deleted_at=NOW() WHERE id = $1`

	resp, err := c.db.Exec(ctx, query,
		req.Id,
	)

	if err != nil {
		return "", err
	}
	if resp.RowsAffected() == 0 {
		return "", pgx.ErrNoRows
	}

	return "Deleted suc", nil
}

func (c *clientRepo) GetAll(ctx context.Context, req *person_service.GetAllClientRequest) (resp *person_service.GetAllClientResponse, err error) {

	var (
		filter  = " WHERE deleted_at IS NULL AND true "
		offsetQ = " OFFSET 0 "
		limit   = " LIMIT 10 "
		offset  = (req.GetPage() - 1) * req.GetLimit()
	)

	info := `
	SELECT
	id,
	first_name,
	last_name,
	phone,
	photo,
	birth_date::text,
	last_order_date::text,
	discount_type,
	total_orders_sum,
	total_orders_count,
	discount_amount,
	created_at::text,
	updated_at::text
	FROM clients
	`

	var count int

	cQ := `
	SELECT
		COUNT(*)
	FROM 
		clients
	`
	if req.GetFirstName() != "" {
		filter += ` AND first_name ILIKE ` + "'%" + req.GetFirstName() + "%'"
	}

	if req.GetLastName() != "" {
		filter += ` AND last_name ILIKE ` + "'%" + req.GetLastName() + "%'"
	}

	if req.GetPhone() != "" {
		filter += ` AND phone ILIKE ` + "'%" + req.GetPhone() + "%'"
	}

	if req.GetDiscountType() != "" {
		filter += ` AND discount_type ILIKE ` + "'%" + req.GetDiscountType() + "%'"
	}

	filter += ` AND total_orders_sum BETWEEN ` + fmt.Sprintf("%f", req.TotalOrderFromSum) + ` AND ` + fmt.Sprintf("%f", req.TotalOrderToSum)

	filter += ` AND discount_amount BETWEEN ` + fmt.Sprintf("%f", req.DiscountFromAmount) + ` AND ` + fmt.Sprintf("%f", req.DiscountToAmount)

	filter += ` AND total_orders_count BETWEEN ` + fmt.Sprintf("%d", req.TotalOrderFromCount) + ` AND ` + fmt.Sprintf("%d", req.TotalOrderToCount)

	filter += ` AND created_at BETWEEN '` + req.CreateFromDate + `' AND '` + req.CreateToDate + `' `

	filter += ` AND last_order_date BETWEEN '` + req.LastOrderFromDate + `' AND '` + req.LastOrderToDate + `' `

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}
	if offset > 0 {
		offsetQ = fmt.Sprintf(" OFFSET %d", offset)
	}

	query := info + filter + limit + offsetQ
	countQuery := cQ + filter

	rows, err := c.db.Query(ctx, query)
	if err != nil {
		return &person_service.GetAllClientResponse{}, err
	}

	err = c.db.QueryRow(context.Background(), countQuery).Scan(&count)

	if err != nil {
		return &person_service.GetAllClientResponse{}, err
	}

	defer rows.Close()

	result := []*person_service.Client{}

	for rows.Next() {

		var client person_service.Client

		err := rows.Scan(
			&client.Id,
			&client.FirstName,
			&client.LastName,
			&client.Phone,
			&client.Photo,
			&client.BirthDate,
			&client.LastOrderDate,
			&client.DiscountType,
			&client.TotalOrderSum,
			&client.TotalOrderCount,
			&client.DiscountAmount,
			&client.CreatedAt,
			&client.UpdatedAt,
		)
		if err != nil {
			return &person_service.GetAllClientResponse{}, err
		}

		result = append(result, &client)

	}

	return &person_service.GetAllClientResponse{Clients: result, Count: int64(count)}, nil

}
