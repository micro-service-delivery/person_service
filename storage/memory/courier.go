package memory

import (
	"context"
	"fmt"
	"person/genproto/person_service"
	"person/packages/helper"

	"github.com/google/uuid"
	"github.com/jackc/pgx"
	"github.com/jackc/pgx/v4/pgxpool"
)

type courierRepo struct {
	db *pgxpool.Pool
}

func NewCourierRepo(db *pgxpool.Pool) *courierRepo {

	return &courierRepo{
		db: db,
	}

}

func (c *courierRepo) Create(ctx context.Context, req *person_service.CreateCourier) (string, error) {

	var id = uuid.NewString()

	hashPas, err := helper.GeneratePasswordHash(req.Password)

	if err != nil {
		return "Error while generete password hash", err
	}

	req.Password = string(hashPas)

	query := `
	INSERT INTO
		couriers (id,branch_id,first_name,last_name,phone,login,password,max_order_count)
	VALUES ($1,$2,$3,$4,$5,$6,$7,$8)`

	_, err = c.db.Exec(ctx, query,
		id,
		req.BranchId,
		req.FirstName,
		req.LastName,
		req.Phone,
		req.Login,
		req.Password,
		req.MaxOrderCount,
	)

	if err != nil {
		fmt.Println("error:", err.Error())
		return "", err
	}

	return id, nil
}

func (c *courierRepo) Update(ctx context.Context, req *person_service.Courier) (string, error) {

	query := `
	UPDATE
		couriers
	SET
		branch_id=$2,first_name=$3,last_name=$4,phone=$5,login=$6,max_order_count=$7,active=$8,updated_at=NOW()
	WHERE
		id=$1`

	resp, err := c.db.Exec(ctx, query,
		req.Id,
		req.BranchId,
		req.FirstName,
		req.LastName,
		req.Phone,
		req.Login,
		req.MaxOrderCount,
		req.Active,
	)

	if err != nil {
		return "", err
	}

	if resp.RowsAffected() == 0 {
		return "", pgx.ErrNoRows
	}

	return "OK", nil
}

func (c *courierRepo) Get(ctx context.Context, req *person_service.IdReqRes) (*person_service.Courier, error) {

	query := `
	SELECT
	id,
	branch_id,
	first_name,
	last_name,
	phone,
	login,
	active,
	max_order_count,
	created_at::text,
	updated_at::text
	FROM couriers
	WHERE id=$1`

	resp := c.db.QueryRow(ctx, query, req.Id)

	var courier person_service.Courier

	err := resp.Scan(
		&courier.Id,
		&courier.BranchId,
		&courier.FirstName,
		&courier.LastName,
		&courier.Phone,
		&courier.Login,
		&courier.Active,
		&courier.MaxOrderCount,
		&courier.CreatedAt,
		&courier.UpdatedAt,
	)

	if err != nil {
		return &person_service.Courier{}, err
	}

	return &courier, nil
}

func (c *courierRepo) Delete(ctx context.Context, req *person_service.IdReqRes) (string, error) {

	query := `UPDATE couriers SET deleted_at=NOW() WHERE id = $1`

	resp, err := c.db.Exec(ctx, query,
		req.Id,
	)

	if err != nil {
		return "", err
	}
	if resp.RowsAffected() == 0 {
		return "", pgx.ErrNoRows
	}

	return "Deleted suc", nil
}

func (c *courierRepo) GetAll(ctx context.Context, req *person_service.GetAllCourierRequest) (resp *person_service.GetAllCourierResponse, err error) {

	var (
		filter  = " WHERE deleted_at IS NULL AND true "
		offsetQ = " OFFSET 0 "
		limit   = " LIMIT 10 "
		offset  = (req.GetPage() - 1) * req.GetLimit()
	)

	info := `
	SELECT
	id,
	branch_id,
	first_name,
	last_name,
	phone,
	login,
	active,
	max_order_count,
	created_at::text,
	updated_at::text
	FROM couriers
	`

	var count int

	cQ := `
	SELECT
		COUNT(*)
	FROM 
		couriers
	`
	if req.GetFirstName() != "" {
		filter += ` AND first_name ILIKE ` + "'%" + req.GetFirstName() + "%'"
	}

	if req.GetLastName() != "" {
		filter += ` AND last_name ILIKE ` + "'%" + req.GetLastName() + "%'"

	}

	if req.GetPhone() != "" {
		filter += ` AND phone ILIKE ` + "'%" + req.GetPhone() + "%'"
	}

	filter += ` AND created_at BETWEEN '` + req.FromDate + `' AND '` + req.ToDate + `' `

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}
	if offset > 0 {
		offsetQ = fmt.Sprintf(" OFFSET %d", offset)
	}

	query := info + filter + limit + offsetQ
	countQuery := cQ + filter

	rows, err := c.db.Query(ctx, query)
	if err != nil {
		return &person_service.GetAllCourierResponse{}, err
	}

	err = c.db.QueryRow(context.Background(), countQuery).Scan(&count)

	if err != nil {
		return &person_service.GetAllCourierResponse{}, err
	}

	defer rows.Close()

	result := []*person_service.Courier{}

	for rows.Next() {

		var courier person_service.Courier

		err := rows.Scan(
			&courier.Id,
			&courier.BranchId,
			&courier.FirstName,
			&courier.LastName,
			&courier.Phone,
			&courier.Login,
			&courier.Active,
			&courier.MaxOrderCount,
			&courier.CreatedAt,
			&courier.UpdatedAt,
		)
		if err != nil {
			return &person_service.GetAllCourierResponse{}, err
		}

		result = append(result, &courier)

	}

	return &person_service.GetAllCourierResponse{Couriers: result, Count: int64(count)}, nil

}

func (c *courierRepo) GetCourierByUserName(ctx context.Context, req *person_service.GetByUserName) (*person_service.Courier, error) {

	query := `
	SELECT
	id,
	branch_id,
	first_name,
	last_name,
	phone,
	login,
	active,
	max_order_count,
	created_at::text,
	updated_at::text
	FROM couriers
	WHERE login=$1 AND active=true`

	resp := c.db.QueryRow(ctx, query, req.Login)

	var courier person_service.Courier

	err := resp.Scan(
		&courier.Id,
		&courier.BranchId,
		&courier.FirstName,
		&courier.LastName,
		&courier.Phone,
		&courier.Login,
		&courier.Active,
		&courier.MaxOrderCount,
		&courier.CreatedAt,
		&courier.UpdatedAt,
	)

	if err != nil {
		return &person_service.Courier{}, err
	}

	return &courier, nil
}
