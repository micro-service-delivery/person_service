package memory

import (
	"context"
	"fmt"
	"person/genproto/person_service"
	"person/packages/helper"

	"github.com/google/uuid"
	"github.com/jackc/pgx"
	"github.com/jackc/pgx/v4/pgxpool"
)

type userRepo struct {
	db *pgxpool.Pool
}

func NewUserRepo(db *pgxpool.Pool) *userRepo {

	return &userRepo{
		db: db,
	}

}

func (c *userRepo) Create(ctx context.Context, req *person_service.CreateUser) (string, error) {

	var id = uuid.NewString()

	hashPas, err := helper.GeneratePasswordHash(req.Password)

	if err != nil {
		return "Error while generete password hash", err
	}

	req.Password = string(hashPas)

	query := `
	INSERT INTO
		users (id,first_name,last_name,phone,login,password)
	VALUES ($1,$2,$3,$4,$5,$6)`

	_, err = c.db.Exec(ctx, query,
		id,
		req.FirstName,
		req.LastName,
		req.Phone,
		req.Login,
		req.Password,
	)

	if err != nil {
		fmt.Println("error:", err.Error())
		return "", err
	}

	return id, nil
}

func (c *userRepo) Update(ctx context.Context, req *person_service.User) (string, error) {

	query := `
	UPDATE
		users
	SET
		first_name=$2,last_name=$3,phone=$4,login=$5,active=$6,updated_at=NOW()
	WHERE
		id=$1`

	resp, err := c.db.Exec(ctx, query,
		req.Id,
		req.FirstName,
		req.LastName,
		req.Phone,
		req.Login,
		req.Active,
	)

	if err != nil {
		return "", err
	}

	if resp.RowsAffected() == 0 {
		return "", pgx.ErrNoRows
	}

	return "OK", nil
}

func (c *userRepo) Get(ctx context.Context, req *person_service.IdReqRes) (*person_service.User, error) {

	query := `
	SELECT
	id,
	first_name,
	last_name,
	phone,
	login,
	active,
	created_at::text,
	updated_at::text
	FROM users
	WHERE id=$1`

	resp := c.db.QueryRow(ctx, query, req.Id)

	var user person_service.User

	err := resp.Scan(
		&user.Id,
		&user.FirstName,
		&user.LastName,
		&user.Phone,
		&user.Login,
		&user.Active,
		&user.CreatedAt,
		&user.UpdatedAt,
	)

	if err != nil {
		return &person_service.User{}, err
	}

	return &user, nil
}

func (c *userRepo) Delete(ctx context.Context, req *person_service.IdReqRes) (string, error) {

	query := `UPDATE users SET deleted_at=NOW() WHERE id = $1`

	resp, err := c.db.Exec(ctx, query,
		req.Id,
	)

	if err != nil {
		return "", err
	}
	if resp.RowsAffected() == 0 {
		return "", pgx.ErrNoRows
	}

	return "Deleted suc", nil
}

func (c *userRepo) GetAll(ctx context.Context, req *person_service.GetAllUserRequest) (resp *person_service.GetAllUserResponse, err error) {

	var (
		filter  = " WHERE deleted_at IS NULL AND true "
		offsetQ = " OFFSET 0 "
		limit   = " LIMIT 10 "
		offset  = (req.GetPage() - 1) * req.GetLimit()
	)

	info := `
	SELECT
	id,
	first_name,
	last_name,
	phone,
	login,
	active,
	created_at::text,
	updated_at::text
	FROM users
	`

	var count int

	cQ := `
	SELECT
		COUNT(*)
	FROM 
		users
	`
	if req.GetFirstName() != "" {
		filter += ` AND first_name ILIKE ` + "'%" + req.GetFirstName() + "%'"
	}

	if req.GetLastName() != "" {
		filter += ` AND last_name ILIKE ` + "'%" + req.GetLastName() + "%'"

	}

	if req.GetPhone() != "" {
		filter += ` AND phone ILIKE ` + "'%" + req.GetPhone() + "%'"
	}

	filter += ` AND created_at BETWEEN '` + req.FromDate + `' AND '` + req.ToDate + `' `

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}
	if offset > 0 {
		offsetQ = fmt.Sprintf(" OFFSET %d", offset)
	}

	query := info + filter + limit + offsetQ
	countQuery := cQ + filter

	rows, err := c.db.Query(ctx, query)
	if err != nil {
		return &person_service.GetAllUserResponse{}, err
	}

	err = c.db.QueryRow(context.Background(), countQuery).Scan(&count)

	if err != nil {
		return &person_service.GetAllUserResponse{}, err
	}

	defer rows.Close()

	result := []*person_service.User{}

	for rows.Next() {

		var user person_service.User

		err := rows.Scan(
			&user.Id,
			&user.FirstName,
			&user.LastName,
			&user.Phone,
			&user.Login,
			&user.Active,
			&user.CreatedAt,
			&user.UpdatedAt,
		)
		if err != nil {
			return &person_service.GetAllUserResponse{}, err
		}

		result = append(result, &user)

	}

	return &person_service.GetAllUserResponse{Users: result, Count: int64(count)}, nil

}

func (c *userRepo) GetUserByUserName(ctx context.Context, req *person_service.GetByUserName) (*person_service.User, error) {

	query := `
	SELECT
	id,
	first_name,
	last_name,
	phone,
	login,
	active,
	created_at::text,
	updated_at::text
	FROM users
	WHERE login=$1 AND active=true`

	resp := c.db.QueryRow(ctx, query, req.Login)

	var user person_service.User

	err := resp.Scan(
		&user.Id,
		&user.FirstName,
		&user.LastName,
		&user.Phone,
		&user.Login,
		&user.Active,
		&user.CreatedAt,
		&user.UpdatedAt,
	)

	if err != nil {
		return &person_service.User{}, err
	}

	return &user, nil
}
