package storage

import (
	"context"
	"person/genproto/person_service"
)

type StoregeI interface {
	User() UserI
	Branch() BranchI
	Client() ClientI
	Courier() CourierI
}

type UserI interface {
	Create(context.Context, *person_service.CreateUser) (string, error)
	Update(context.Context, *person_service.User) (string, error)
	Get(context.Context, *person_service.IdReqRes) (*person_service.User, error)
	GetAll(context.Context, *person_service.GetAllUserRequest) (*person_service.GetAllUserResponse, error)
	Delete(context.Context, *person_service.IdReqRes) (string, error)
	GetUserByUserName(context.Context, *person_service.GetByUserName) (*person_service.User, error)
}

type BranchI interface {
	Create(context.Context, *person_service.CreateBranch) (string, error)
	Update(context.Context, *person_service.Branch) (string, error)
	Get(context.Context, *person_service.IdReqRes) (*person_service.Branch, error)
	GetAll(context.Context, *person_service.GetAllBranchRequest) (*person_service.GetAllBranchResponse, error)
	Delete(context.Context, *person_service.IdReqRes) (string, error)
	GetAllActive(context.Context, *person_service.GetAllActiveBranchRequest) (*person_service.GetAllBranchResponse, error)
}

type ClientI interface {
	Create(context.Context, *person_service.CreateClient) (string, error)
	Update(context.Context, *person_service.Client) (string, error)
	Get(context.Context, *person_service.IdReqRes) (*person_service.Client, error)
	GetAll(context.Context, *person_service.GetAllClientRequest) (*person_service.GetAllClientResponse, error)
	Delete(context.Context, *person_service.IdReqRes) (string, error)
}

type CourierI interface {
	Create(context.Context, *person_service.CreateCourier) (string, error)
	Update(context.Context, *person_service.Courier) (string, error)
	Get(context.Context, *person_service.IdReqRes) (*person_service.Courier, error)
	GetAll(context.Context, *person_service.GetAllCourierRequest) (*person_service.GetAllCourierResponse, error)
	Delete(context.Context, *person_service.IdReqRes) (string, error)
	GetCourierByUserName(context.Context, *person_service.GetByUserName) (*person_service.Courier, error)
}
